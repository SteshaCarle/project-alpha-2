from django.shortcuts import render, redirect
from .models import Project, Quote
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm
import random

@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    random_seed = random.randint(1, 1000)
    picsum_url = "https://picsum.photos/seed/{random_seed}/200"
    quotes = Quote.objects.all()
    context = {
        "projects": projects,
        "picsum_url": picsum_url,
        "quotes": quotes,
    }
    return render(request, "projects/list_projects.html", context)

@login_required
def show_project(request, id):
    details = Project.objects.get(id=id)
    tasks = details.tasks.all()
    quotes = Quote.objects.all()
    context = {
        "details": details,
        "tasks": tasks,
        "quotes": quotes,
    }
    return render(request, "projects/show_project.html", context)

@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)

def show_quotes(request):
    quotes = Quote.objects.all()
    context = {
        'quotes': quotes,
    }
    return render(request, "projects/quotes.html", context)
